<?php

require_once 'vendor/autoload.php';

use awaluk\NextbikeClient\HttpClient;
use awaluk\NextbikeClient\Nextbike;


function convert_umlauts($str){
 $tempstr = ["Ä" => "AE", "Ö" => "OE", "Ü" => "UE", "ä" => "ae", "ö" => "oe", "ü" => "ue"];
 return strtr($str, $tempstr);
}

function get_permalink($string) {

    $string = preg_replace('/\s/', '', $string);
    $string = convert_umlauts($string);
    $string = preg_replace('/\./', '', $string);
    $string = preg_replace('/\(/', '', $string);
    $string = preg_replace('/\)/', '', $string);
    $string = strtolower($string);

    return $string;
}

function remove_unnecessary_part_city($string) {

    $pos = strpos($string, ",");

    if($pos) {
        return substr($string, 0, strpos($string, ","));
    } else {
        return $string;
    }
}

function get_inhab_sort_desc_pos($data_inhabitants, $data_bikes) {

    //sind bereits nach groesse sortiert

    $counter = 1;

    $inhab_sort_desc_pos = [];

    foreach($data_inhabitants as $city_perm => $inhabitants) {

        if($data_bikes[$city_perm]) {
            $inhab_sort_desc_pos[$city_perm] = $counter;
            $counter++;
        }
    }

    return $inhab_sort_desc_pos;

}

function get_bikes_sort_desc_pos($data_bikes) {

    $temp_data_bikes = $data_bikes;
    uasort($temp_data_bikes, function ($a, $b) {
        if ($a == $b) {
            return 0;
        }

        $a = intval($a);
        $b = intval($b);
        return ($a > $b) ? -1 : 1;
    });

    $counter = 1;

    $bikes_sort_desc_pos = [];

    foreach($temp_data_bikes as $city_perm => $bikes) {
        $bikes_sort_desc_pos[$city_perm] = $counter;
        $counter++;
    }

    return $bikes_sort_desc_pos;


}

//Einwohner

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$reader->setLoadSheetsOnly(["Städte"]);
$spreadsheet = $reader->load("05-staedte.xlsx");

$cityColumn = "G";
$inhabitantsColumn = "J";
$first_row = 8;
$last_row = 2068;

$data_inhabitants = [];

$currentCellCityValue = $cityColumn . $row;

for($i = $first_row; $i <= $last_row; $i++) {
    $currentCellCityValue = remove_unnecessary_part_city($spreadsheet->getActiveSheet()->getCell($cityColumn . $i)->getValue());
    $currentCellInhabitantsValue = $spreadsheet->getActiveSheet()->getCell($inhabitantsColumn . $i)->getValue();

    $data_inhabitants[get_permalink($currentCellCityValue)] = $currentCellInhabitantsValue;
}

$data = [];

//Nextbike Fahrräder

$httpClient = new HttpClient();
$nextbike = new Nextbike($httpClient);
$systems = $nextbike->getSystems()->getAll();

/** @var \awaluk\NextbikeClient\Collection\CityCollection $cities */
$cities = [];
$bikes_amount = [];

foreach ($systems as $system) {

    if($system->getCountryCode() === "DE") {

        $cities = $system->getCityCollection()->getAll();
        break;
    }
}

$data_bikes = [];
foreach($cities as $city) {
    $data_bikes[get_permalink($city->getName())] = $city->getAllBikesAmount();
}

$inhab_sort_desc_pos = get_inhab_sort_desc_pos($data_inhabitants, $data_bikes);
$bikes_sort_desc_pos = get_bikes_sort_desc_pos($data_bikes);

//Merge both

foreach($cities as $city) {

    $city_perm = get_permalink($city->getName());

    $data[] = [
        convert_umlauts($city->getName()),
        $data_inhabitants[$city_perm],
        $city->getAllBikesAmount(),
        $inhab_sort_desc_pos[$city_perm],
        $bikes_sort_desc_pos[$city_perm],
        abs($inhab_sort_desc_pos[$city_perm] - $bikes_sort_desc_pos[$city_perm]),
        $city_perm
    ];
}

//File saving

$csv_file = fopen('data.csv', 'w');
fputcsv($csv_file, ['city', 'inhabitants', 'amountBikes', 'inhabSortDescPos', 'bikesSortDescPos', 'diffInhabBikes', 'permalink']);



foreach ($data as $row)
{
    fputcsv($csv_file, $row);
}

fclose($csv_file);

//successful

echo "data.csv created successfully";